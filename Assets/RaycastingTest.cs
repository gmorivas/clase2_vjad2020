﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastingTest : MonoBehaviour
{
    public Camera camera;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0)) { 

            // ray casting - lanzar rayo
            Ray fromMouse = camera.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            if (Physics.Raycast(fromMouse, out hit)) {

                print("HIT SOMETHING! " + hit.transform.name + " " +
                    hit.point);
            }
        }
    }
}
