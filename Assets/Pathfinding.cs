﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding 
{
    public static List<Node> Breadthwise(Node start, Node end) {

        //2 structures - working queue, visited list
        Queue<Node> work = new Queue<Node>();
        List<Node> visited = new List<Node>();

        start.history = new List<Node>();

        work.Enqueue(start);
        visited.Add(start);

        // do the search if we still have available nodes
        while (work.Count > 0) {
            Node current = work.Dequeue();

            if (current == end)
            {
                List<Node> result = new List<Node>(current.history);
                result.Add(current);
                return result;
            }
            else 
            {
                // the current working node is not the target
                // process neighbors
                for (int i = 0; i < current.neighbors.Length; i++) {

                    Node currentChild = current.neighbors[i];

                    // only process if we haven't been here before
                    if (!visited.Contains(currentChild)) {

                        // process history
                        currentChild.history = new List<Node>(current.history);
                        currentChild.history.Add(current);

                        work.Enqueue(currentChild);
                        visited.Add(currentChild);
                    }
                }
            }
        }

        // if we fall here, there was no path
        return null; 
    }

    public static List<Node> Depthwise(Node start, Node end) {

        //2 structures - working queue, visited list
        Stack<Node> work = new Stack<Node>();
        List<Node> visited = new List<Node>();

        start.history = new List<Node>();

        work.Push(start);
        visited.Add(start);

        // do the search if we still have available nodes
        while (work.Count > 0)
        {
            Node current = work.Pop();

            if (current == end)
            {
                List<Node> result = new List<Node>(current.history);
                result.Add(current);
                return result;
            }
            else
            {
                // the current working node is not the target
                // process neighbors
                for (int i = 0; i < current.neighbors.Length; i++)
                {

                    Node currentChild = current.neighbors[i];

                    // only process if we haven't been here before
                    if (!visited.Contains(currentChild))
                    {

                        // process history
                        currentChild.history = new List<Node>(current.history);
                        currentChild.history.Add(current);

                        work.Push(currentChild);
                        visited.Add(currentChild);
                    }
                }
            }
        }

        // if we fall here, there was no path
        return null;

    }

    public static List<Node> AStar(Node start, Node end) {

        // 2 estructuras
        List<Node> work = new List<Node>();
        List<Node> visited = new List<Node>();

        start.history = new List<Node>();
        start.G = 0;
        start.H = Vector3.Distance(
                start.transform.position,
                end.transform.position
            );

        work.Add(start);
        visited.Add(start);

        //special case: the target node is the starting point
        // sounds dumb but it might happen!
        if (start == end) {
            List<Node> path = new List<Node>();
            path.Add(start);
            return path;
        }

        while (work.Count > 0) {

            // selección del siguiente!
            Node smaller = work[0];
            foreach (Node current in work) {
                if (current.F < smaller.F) {
                    smaller = current;
                }
            }

            // importante!
            work.Remove(smaller);

            for (int i = 0; i < smaller.neighbors.Length; i++) {

                Node currentChild = smaller.neighbors[i];

                // optimizacion - checar si es resultado
                if (currentChild == end)
                {

                    //currentChild.history.Add(smaller);
                    List<Node> result = new List<Node>(smaller.history);
                    result.Add(smaller);
                    result.Add(currentChild);
                    return result;
                }

                if (!visited.Contains(currentChild)) { 

                    // calcular g, h
                    currentChild.G = smaller.G +
                        Vector3.Distance(
                            currentChild.transform.position,
                            smaller.transform.position
                        ); 
                    currentChild.H = Vector3.Distance(
                            currentChild.transform.position,
                            end.transform.position
                        );

                    // procesar historia
                    currentChild.history = new List<Node>(smaller.history);
                    currentChild.history.Add(smaller);

                    // agregar a estructuras
                    work.Add(currentChild);
                    visited.Add(currentChild);
                }
            }
        }

        return null; 
    
    }
}
