﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{

    public Node[] path;
    public float speed = 5;
    public float threshold; // umbral

    private int current;

    // Start is called before the first frame update
    void Start()
    {
        current = 0;
        StartCoroutine(DistanceCheck());
    }

    // Update is called once per frame
    void Update()
    {

        transform.LookAt(path[current].transform);
        transform.Translate(speed * transform.forward * Time.deltaTime, Space.World);
    }

    IEnumerator DistanceCheck() {

        while (true) {

            // is going to be superhard to get exactly to the point 
            float distance = Vector3.Distance(
                transform.position,
                path[current].transform.position
                );

            // we get there when we are close enough
            if (distance < threshold) {

                // move to the next one
                current++;

                // if out of bounds return to 0
                current %= path.Length;
            }

            yield return new WaitForSeconds(0.18f);
        }
    }

    private void OnMouseDown()
    {
        print("CLICK!");
    }
}
