﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{

    public Node[] neighbors;

    // how do you know how you got here?
    public List<Node> history;

    public float G
    {
        get;
        set;
    }

    public float H
    {
        get;
        set;
    }

    public float F
    {
        get {
            return G + H;
        }
    }

    // Let's do GIZMOS!
    // 2 points where we can draw gizmos
    // - non-selected object
    // Gizmos methods run on editor, not on gameplay
    private void OnDrawGizmos()
    {

        //set color first
        Gizmos.color = Color.magenta;

        // anything we draw will be of that color until we
        // change it again
        Gizmos.DrawSphere(transform.position, 1);

        //actually draw line between neighbors
        Gizmos.color = Color.yellow;

        for (int i = 0; i < neighbors.Length; i++) {
            if (neighbors[i] == null)
                continue;

            Gizmos.DrawLine(
                transform.position,
                neighbors[i].transform.position
                );
        }
    }

    // - selected object
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;

        Gizmos.DrawWireSphere(transform.position, 1);
    }

}
