﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PFTest : MonoBehaviour
{

    public Node start, end;

    // Start is called before the first frame update
    void Start()
    {

        //List<Node> result = Pathfinding.Breadthwise(start, end);
        //List<Node> result2 = Pathfinding.Depthwise(start, end);
        List<Node> result3 = Pathfinding.AStar(start, end);
        
        /*
        foreach (Node current in result) {
            print("BREADTHWISE PATH: " + current.transform.name);
        }*/

        /*
        for (int i = 0; i < result2.Count; i++) {

            print("DEPTHWISE PATH: " + result2[i]);
        }
        */
        foreach (Node current in result3) {
            print("A STAR: "  + current);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
